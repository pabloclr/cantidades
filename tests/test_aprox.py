#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import quantities

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

class TestOpAdd(unittest.TestCase):

    def test_simple(self):
        with patch.object(sys, 'argv', ['milk', '4']):
            quantities.op_add()
        self.assertEqual({'milk': 4}, quantities.items)

class TestOpItems(unittest.TestCase):

    def test_simple(self):
        quantities.items = {'milk': 4}
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            quantities.op_items()
        output = stdout.getvalue()
        self.assertEqual('Items: milk\n' , output)

    def test_long(self):
        quantities.items = {'milk': 4, 'sugar': 3, 'bread': 1, 'ham': 5}
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            quantities.op_items()
        output = stdout.getvalue()
        self.assertEqual('Items: milk sugar bread ham\n' , output)

class TestOpAll(unittest.TestCase):

    def test_simple(self):
        quantities.items = {'milk': 4}
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            quantities.op_all()
        output = stdout.getvalue()
        self.assertEqual('All: milk (4)\n' , output)

    def test_long(self):
        quantities.items = {'milk': 4, 'sugar': 3, 'bread': 1, 'ham': 5}
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            quantities.op_all()
        output = stdout.getvalue()
        self.assertEqual('All: milk (4) sugar (3) bread (1) ham (5)\n' , output)

class TestOpSum(unittest.TestCase):

    def test_simple(self):
        quantities.items = {'milk': 4}
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            quantities.op_sum()
        output = stdout.getvalue()
        self.assertEqual('Sum: 4\n' , output)

    def test_long(self):
        quantities.items = {'milk': 4, 'sugar': 3, 'bread': 1, 'ham': 5}
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            quantities.op_sum()
        output = stdout.getvalue()
        self.assertEqual('Sum: 13\n' , output)

expected_misc = """Sum: 4
All: milk (4) bread (3) sugar (6)
Sum: 13
"""

class TestMain(unittest.TestCase):

    def setUp(self):
        quantities.items = {}

    def test_add(self):
        with patch.object(sys, 'argv', ['quantities.py', 'add', 'milk', '4']):
            quantities.main()
        self.assertEqual({'milk': 4}, quantities.items)

    def test_items(self):
        with patch.object(sys, 'argv', ['quantities.py', 'add', 'milk', '4', 'items']):
            stdout = StringIO()
            with contextlib.redirect_stdout(stdout):
                quantities.main()
        output = stdout.getvalue()
        self.assertEqual({'milk': 4}, quantities.items)
        self.assertEqual('Items: milk\n' , output)

    def test_all(self):
        with patch.object(sys, 'argv', ['quantities.py', 'add', 'milk', '4', 'all']):
            stdout = StringIO()
            with contextlib.redirect_stdout(stdout):
                quantities.main()
        output = stdout.getvalue()
        self.assertEqual({'milk': 4}, quantities.items)
        self.assertEqual('All: milk (4)\n' , output)

    def test_sum(self):
        with patch.object(sys, 'argv', ['quantities.py', 'add', 'milk', '4', 'sum']):
            stdout = StringIO()
            with contextlib.redirect_stdout(stdout):
                quantities.main()
        output = stdout.getvalue()
        self.assertEqual({'milk': 4}, quantities.items)
        self.assertEqual('Sum: 4\n' , output)

    def test_misc(self):
        with patch.object(sys, 'argv',
                          ['quantities.py', 'add', 'milk', '4', 'sum',
                           'add', 'bread', '3', 'add', 'sugar', '6',
                           'all', 'sum']):
            stdout = StringIO()
            with contextlib.redirect_stdout(stdout):
                quantities.main()
        output = stdout.getvalue()
        self.assertEqual({'bread': 3, 'milk': 4, 'sugar': 6},
                         quantities.items)
        self.assertEqual(expected_misc , output)


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
